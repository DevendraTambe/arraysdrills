function flatten(elements) {
    let flatArray = [];
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            flatArray.push(...flatten(elements[index]))
        } else {
            flatArray.push(elements[index]);
        }
    }
    return flatArray;
}
module.exports = flatten;
