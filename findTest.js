const items = require('../arrays.js');
let testFind = require('./findProblem.js');

function cb(value) {
    if ((value % 2) == 0) {
        return true;
    }
}

let findElement = testFind(items, cb);
let actualOutput = 2;
if (findElement === actualOutput) {
    console.log(findElement);
} else {
    console.log('Wrong Output');
}
