function map(elements, cb) {
    let mapResult = [];
    for (let index = 0; index < elements.length; index++) {
        mapResult.push(cb(elements[index]));
    }
    return mapResult;
}

module.exports = map;