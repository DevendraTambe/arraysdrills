let testReduce = require('./reduceProblem');
const items = require('../arrays.js');

function cb(value, reduce) {
    reduce += value;
    return reduce;
}
let arraySum = testReduce(items, cb);
if (arraySum === 20) {
    console.log(arraySum);
} else {
    console.log("Wrong Output");
}