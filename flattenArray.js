const nestedArray = [1, [2],
    [
        [3]
    ],
    [
        [
            [4]
        ]
    ]
]; // use this to test 'flatten'
module.exports = nestedArray;