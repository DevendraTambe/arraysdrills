function each(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index]);
    }
    return undefined;
}

module.exports = each;