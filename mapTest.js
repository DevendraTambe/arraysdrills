const items = require('../arrays.js');
let testMap = require('./mapProblem');
let mapArray = testMap(items, cb);

function cb(value) {
    return value * value;
}

let testOutput = testMap(items, cb);
let flag = true;

for (let index = 0; index < items.length; index++) {
    if (testOutput[index] !== (items[index] * items[index])) {
        flag = false;
        break;
    }
}

if (flag === true) {
    console.log(mapArray);
} else {
    console.log('Wrong Output');
}