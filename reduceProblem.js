function reduce(elements, cb, startingValue) {
    let sum;
    if(startingValue){
        sum = startingValue;
    }else{
        sum = elements.shift();
    }
    for (let index = 0; index < elements.length; index++) {
        sum = cb(sum,elements[index]);
    }
    return sum;
}
module.exports = reduce;
