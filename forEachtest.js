let testEach = require('./forEachproblem.js');
const items = require('../arrays.js');

function cb(value) {
    console.log(value);
}

let testOutput = testEach(items, cb);
let actualOutput = [1, 2, 3, 4, 5, 5];

if (testOutput !== undefined) {
    console.log("Wrong Output");
}