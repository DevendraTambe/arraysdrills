function filter(elements, cb) {
    let filterElements = [];
    for (let value in elements) {
        if (cb(value)) {
            filterElements.push(value);
        }
    }
    return filterElements;
}

module.exports = filter;