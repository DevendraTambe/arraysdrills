let testFilter = require('./filterProblem');
const items = require('../arrays.js');

function cb(value) {
    if (value % 2 !== 0) {
        return value;
    }
}
let filterArray = testFilter(items, cb);
let actualOutput = ['1', '3', '5'];
if (filterArray && actualOutput) {
    console.log(filterArray);
} else {
    console.log('Different Output');
}